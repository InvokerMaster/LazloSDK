Pod::Spec.new do |s|
  s.name = 'LazloSDK'
  s.version = '0.0.1'
  s.summary = 'LazloSDK'
  s.homepage = 'https://lazlo326.com'
  s.ios.deployment_target = '12.1'
  s.source = { :git => 'https://github.com/operationslazlo326/lazlo.shopping.sdk.ios', :branch=>'development' }
  s.authors = 'Invoker'
  s.license = { :type => 'GPL 3.0' }
  s.source_files = 'swagger/swift4/SwaggerClient/classes/**/*.swift'
  s.dependency 'Alamofire', '~> 4.5.0'
end
        
